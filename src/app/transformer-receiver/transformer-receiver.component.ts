import {Component} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {select, Store} from '@ngrx/store';
import {AppState} from '../app-state';
import {AsyncValueState} from '../action/async-value';
import {ThresholdState} from '../action/threshold';

@Component({
  selector: 'test-transformer-receiver',
  templateUrl: './transformer-receiver.component.html',
  styleUrls: ['./transformer-receiver.component.scss']
})
export class TransformerReceiverComponent {
  public asyncValue$: Observable<AsyncValueState>;
  public threshold$: Observable<ThresholdState>;
  public ids$: Observable<number[]>;
  public names$: Observable<string[]>;

  constructor(private store: Store<AppState>) {
    this.asyncValue$ = store.pipe(select('asyncValue'));
    this.threshold$ = store.pipe(select('threshold'));
    this.ids$ = store.pipe(select((appState: AppState) => appState.items.map((item) => item.id)));
    this.names$ = store.pipe(select((appState: AppState) => appState.items.map((item) => item.name)));
  }
}
