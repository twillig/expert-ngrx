import {Component} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {AppState} from '../app-state';
import {Observable} from 'rxjs/Observable';
import {ThresholdState} from '../action/threshold';
import {ItemsState} from '../action/items';
import {AsyncValueState} from '../action/async-value';

@Component({
  selector: 'test-receiver',
  templateUrl: './receiver.component.html',
  styleUrls: ['./receiver.component.scss']
})
export class ReceiverComponent {
  public asyncValue$: Observable<AsyncValueState>;
  public threshold$: Observable<ThresholdState>;
  public items$: Observable<ItemsState>;

  constructor(private store: Store<AppState>) {
    this.asyncValue$ = store.pipe(select('asyncValue'));
    this.threshold$ = store.pipe(select('threshold'));
    this.items$ = store.pipe(select('items'));
  }
}
