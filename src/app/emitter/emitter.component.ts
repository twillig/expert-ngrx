import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../app-state';
import {DecreaseIdsBy10Action, IncreaseIdsBy10Action, PrefixNameAction} from '../action/items';
import {RequestSetValueAction} from '../action/async-value';

@Component({
  selector: 'test-emitter',
  templateUrl: './emitter.component.html',
  styleUrls: ['./emitter.component.scss']
})
export class EmitterComponent {
  constructor(private store: Store<AppState>) {

  }

  onPrefixItems() {
    this.store.dispatch(new PrefixNameAction('prefix'));
  }

  onRemovePrefixItems() {
    this.store.dispatch(new PrefixNameAction(''));
  }

  onIncreaseIdBy10() {
    this.store.dispatch(new IncreaseIdsBy10Action());
  }

  onDecreaseIdBy10() {
    this.store.dispatch(new DecreaseIdsBy10Action());
  }

  onIncreaseAsyncBy10() {
    this.store.dispatch(new RequestSetValueAction(10));
  }
}
