import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Action, State, Store} from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {AsyncStateName, AsyncValueActionType, RequestSetValueAction, ResponseSetValueAction} from '../action/async-value';
import {mergeMap} from 'rxjs/operators';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import {AppState} from '../app-state';

@Injectable()
export class AsyncValueEffect {
  @Effect() setValue$: Observable<Action> = this.actions$.pipe(
    ofType(AsyncValueActionType.REQUEST_SET_VALUE),
    mergeMap((action: RequestSetValueAction) => {
      return Observable.of(
        new ResponseSetValueAction(this.state.getValue().asyncValue.value + action.value, AsyncStateName.RESOLVED)
      ).delay(1000);
    })
  );

  constructor(private actions$: Actions, private state: State<AppState>) {}
}
