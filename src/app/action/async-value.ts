import {Action} from '@ngrx/store';

export enum AsyncStateName {
  PENDING = 'PENDING',
  RESOLVED = 'RESOLVED',
  REJECTED = 'REJECTED'
}

export interface AsyncValueState {
  value: number;
  state: AsyncStateName;
}

export enum AsyncValueActionType {
  REQUEST_SET_VALUE = 'REQUEST_SET_VALUE',
  RESPONSE_SET_VALUE = 'RESPONSE_SET_VALUE'
}

export class RequestSetValueAction implements Action {
  public type: string = AsyncValueActionType.REQUEST_SET_VALUE;

  constructor(public value: number) {}
}

export class ResponseSetValueAction implements Action {
  public type: string = AsyncValueActionType.RESPONSE_SET_VALUE;

  constructor(public value: number, public state: AsyncStateName) {}
}

export function asyncValueReducer(state: AsyncValueState = {
  value: 0,
  state: AsyncStateName.RESOLVED
}, action: Action): AsyncValueState {
  switch (action.type) {
    case AsyncValueActionType.REQUEST_SET_VALUE:
      if (action instanceof RequestSetValueAction) {
        state.state = AsyncStateName.PENDING;
      }
      break;
    case AsyncValueActionType.RESPONSE_SET_VALUE:
      if (action instanceof ResponseSetValueAction) {
        state.value = action.value;
        state.state = action.state;
      }
      break;
  }

  return state;
}
