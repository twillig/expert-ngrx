import {Action} from '@ngrx/store';

export type ItemsState = Array<{
  id: number;
  name: string;
}>;

export enum ItemsActionType {
  PREFIX_NAME = 'PREFIX_NAME',
  INCREASE_IDS_BY_10 = 'INCREASE_IDS_BY_10',
  DECREASE_IDS_BY_10 = 'DECREASE_IDS_BY_10'
}

export class PrefixNameAction implements Action {
  public type: string = ItemsActionType.PREFIX_NAME;

  constructor(public prefix: string) {}
}

export class IncreaseIdsBy10Action implements Action {
  public type: string = ItemsActionType.INCREASE_IDS_BY_10;

  constructor() {}
}

export class DecreaseIdsBy10Action implements Action {
  public type: string = ItemsActionType.DECREASE_IDS_BY_10;

  constructor() {}
}

function prefixNames(prefix: string, state: ItemsState): ItemsState {
  for (const item of state) {
    const indexUnderscore = item.name.indexOf('_');

    if (indexUnderscore === -1) {
      if (prefix) {
        item.name = prefix + '_' + item.name;
      }
    } else {
      if (prefix) {
        item.name = prefix + '_' + item.name.substring(indexUnderscore + 1);
      } else {
        item.name = item.name.substring(indexUnderscore + 1);
      }
    }
  }

  return state;
}

function addToIds(amount: number, state: ItemsState): ItemsState {
  for (const item of state) {
    item.id += amount;
  }

  return state;
}

export function itemsReducer(state: ItemsState = [
  {
    id: 234,
    name: 'ab'
  },
  {
    id: 345,
    name: 'bc'
  },
  {
    id: 456,
    name: 'cd'
  }
], action: Action): ItemsState {
  switch (action.type) {
    case ItemsActionType.PREFIX_NAME:
      if (action instanceof PrefixNameAction) {
        state = prefixNames(action.prefix, state);
      }
      break;
    case ItemsActionType.INCREASE_IDS_BY_10:
      state = addToIds(10, state);
      break;
    case ItemsActionType.DECREASE_IDS_BY_10:
      state = addToIds(-10, state);
      break;
  }

  return state;
}
