import {Action} from '@ngrx/store';

export type ThresholdState = number;

export enum ThresholdActionType {
  SET_THRESHOLD = 'SET_THRESHOLD'
}

export class SetThresholdAction implements Action {
  public type: string = ThresholdActionType.SET_THRESHOLD;

  constructor(public threshold: number) {}
}

export function thresholdReducer(state: ThresholdState = 765, action: Action): ThresholdState {
  switch (action.type) {
    case ThresholdActionType.SET_THRESHOLD:
      if (action instanceof SetThresholdAction) {
        state = action.threshold;
      }
      break;
  }

  return state;
}
