import {Action, ActionReducerMap} from '@ngrx/store';
import {itemsReducer, ItemsState} from './action/items';
import {thresholdReducer, ThresholdState} from './action/threshold';
import {asyncValueReducer, AsyncValueState} from './action/async-value';

export interface AppState {
  asyncValue: AsyncValueState;
  threshold: ThresholdState;
  items: ItemsState;
}

export const appReducers: ActionReducerMap<AppState, Action> = {
  asyncValue: asyncValueReducer,
  threshold: thresholdReducer,
  items: itemsReducer
};
