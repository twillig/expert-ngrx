import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {StoreModule} from '@ngrx/store';

import { AppComponent } from './app.component';
import {EmitterComponent} from './emitter/emitter.component';
import {ReceiverComponent} from './receiver/receiver.component';
import {TransformerReceiverComponent} from './transformer-receiver/transformer-receiver.component';
import {appReducers} from './app-state';
import {EffectsModule} from '@ngrx/effects';
import {AsyncValueEffect} from './effect/async-value.effect';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppComponent,
    EmitterComponent,
    ReceiverComponent,
    TransformerReceiverComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([AsyncValueEffect]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
